package com.accenture.devops.hackathon.repository;

import com.accenture.devops.hackathon.model.Todo;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface TodoRepository extends PagingAndSortingRepository<Todo, String> {}
